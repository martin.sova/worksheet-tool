import _axios from 'axios';

import { formatDate } from '@/utils/format-date';
import { getUTCDate } from '@/utils/get-utc-date';
import { getIterableApiResults } from '@/utils/iterable-api-results';
import { configManager } from '@lib/config-manager';

/**
 * GitLab Events API
 * @link https://docs.gitlab.com/ee/api/events.html
 */

export interface IGitlabUserEvent {
  created_at: string;
  project_id: number;
  project_name: string;
  push_data: {
    commit_count: number | null;
    action: 'pushed' | 'created';
    ref_type: 'branch' | 'tag';
    ref: string;
    commit_title: string;
  };
}

export interface IGitlabProjectDetail {
  id: number;
  name: string;
}

const createGitLabApi = (url: string, personalAccessToken: string) =>
  _axios.create({
    baseURL: url,
    headers: {
      'PRIVATE-TOKEN': personalAccessToken,
    },
  });

const PROJECT_CACHE: Record<number, IGitlabProjectDetail> = {};

export const createGitlabConnector = (month: number, year: number) => {
  const config = configManager.config.services.gitlab;
  const _gitlabApi = createGitLabApi(config.url, config.token);

  const _getUserPushedEvents = (after: string, before: string, page = 0, resultCount = '100') =>
    _gitlabApi.get<IGitlabUserEvent[]>(`/users/${config.user}/events`, {
      params: {
        action: 'pushed',
        before: before,
        after: after,
        page: page,
        per_page: resultCount,
      },
    });

  const _getProjectById = async (id: number, cache = true) => {
    if (cache && id in PROJECT_CACHE) {
      return PROJECT_CACHE[id];
    }

    try {
      const { data } = await _gitlabApi.get<IGitlabProjectDetail>(`/projects/${id}`);
      if (cache) PROJECT_CACHE[id] = data;
      return data;
    } catch (error) {
      //@todo error handling
    }
  };

  return Object.freeze({
    async getUserPushedCommitEvents() {
      const after = formatDate(getUTCDate(month - 1, year, 1));
      const before = formatDate(getUTCDate(month, year, 0)); // The Very Last Day Of Month
      const results = await getIterableApiResults((page) =>
        _getUserPushedEvents(after, before, page),
      );

      for (const index in results) {
        const event = results[index];
        const project = await _getProjectById(+event.project_id);
        event['project_name'] = project?.name || '';
      }

      return results;
    },
  });
};
