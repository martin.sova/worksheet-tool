import inquirer from 'inquirer';
import { createRangeArray } from '@/utils/create-range-array';
import locales from '@/locales';

export const askLoadProfile = async (profiles: Record<string, string | null>[]) => {
  const response = await inquirer.prompt({
    name: 'loadProfile',
    type: 'list',
    choices: profiles,
    message: locales.promptQuestions.profile(profiles.length),
  });
  return {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    loadProfile: profiles.find(({ name }) => name === response.loadProfile)!,
  };
};

export const askForName = async () => {
  const response = await inquirer.prompt({
    name: 'name',
    type: 'input',
    message: locales.promptQuestions.name,
  });
  return response;
};

export const askForIN = async () => {
  const response = await inquirer.prompt({
    name: 'IN',
    type: 'input',
    message: locales.promptQuestions.in,
  });
  return response;
};

export const askForSaveProfile = async () => {
  const response = await inquirer.prompt({
    name: 'saveProfile',
    type: 'confirm',
    message: locales.promptQuestions.saveProfile,
    default() {
      return true;
    },
  });
  return response;
};

export const askForYear = async () => {
  const response = await inquirer.prompt({
    name: 'year',
    type: 'list',
    message: locales.promptQuestions.year,
    choices: createRangeArray(30, 2021),
    default() {
      const currentYear = new Date().getFullYear();
      return currentYear.toString();
    },
  });
  return response;
};

export const askForMonth = async () => {
  const response = await inquirer.prompt({
    name: 'month',
    type: 'list',
    message: locales.promptQuestions.month,
    choices: createRangeArray(12, 1),
    default() {
      const currentMonth = new Date().getMonth() - 1;
      return currentMonth;
    },
  });
  return response;
};

export const askForWorkOverHolidays = async (holidays: string[]) => {
  if (holidays.length === 0)
    return {
      excludedHolidays: [],
    };

  const { workedOverHolidays } = await inquirer.prompt({
    name: 'workedOverHolidays',
    type: 'confirm',
    message: locales.promptQuestions.workOverHolidays(holidays.length),
    default() {
      return false;
    },
  });

  if (!workedOverHolidays)
    return {
      excludedHolidays: [...holidays],
    };

  const { selectedHolidays } = await inquirer.prompt({
    name: 'selectedHolidays',
    type: 'checkbox',
    message: locales.promptQuestions.selectHolidays,
    choices: holidays,
    default() {
      return false;
    },
  });

  return {
    excludedHolidays: holidays.filter((holiday) => !selectedHolidays.includes(holiday)),
  };
};

export const askForWorkOverWeekends = async (weekends: string[]) => {
  const { workedOverWeekends } = await inquirer.prompt({
    name: 'workedOverWeekends',
    type: 'confirm',
    message: locales.promptQuestions.workOverWeekends,
    default() {
      return false;
    },
  });

  if (!workedOverWeekends)
    return {
      excludedWeekends: [...weekends],
    };

  const { selectedWeekends } = await inquirer.prompt({
    name: 'selectedWeekends',
    type: 'checkbox',
    message: locales.promptQuestions.selectHolidays,
    choices: weekends,
    default() {
      return false;
    },
  });

  return {
    excludedWeekends: weekends.filter((weekend) => !selectedWeekends.includes(weekend)),
  };
};

export const askForExcludeDays = async (dates: string[]) => {
  const { shouldExcludeDays } = await inquirer.prompt({
    name: 'shouldExcludeDays',
    type: 'confirm',
    message: locales.promptQuestions.shouldExcludeDays,
    default() {
      return false;
    },
  });

  if (!shouldExcludeDays)
    return {
      excludedDays: [],
    };

  const { excludedDays } = await inquirer.prompt({
    name: 'excludedDays',
    type: 'checkbox',
    message: locales.promptQuestions.selectHolidays,
    choices: dates,
    default() {
      return false;
    },
  });

  return {
    excludedDays: excludedDays,
  };
};

export const askForPrefillDescriptionFromGitlab = async () => {
  const response = await inquirer.prompt({
    name: 'prefillGitlabActivity',
    type: 'confirm',
    message: locales.promptQuestions.prefillGitlabActivity,
    default() {
      return true;
    },
  });
  return response;
};

export const askForEditDataConfirm = async () => {
  const response = await inquirer.prompt({
    name: 'editDataConfirm',
    type: 'confirm',
    message: locales.promptQuestions.editModeConfirm,
    default() {
      return false;
    },
  });
  return response;
};

export const askForEditData = async (data: string) => {
  const response = await inquirer.prompt({
    name: 'editData',
    type: 'editor',
    default: data,
  });
  return response;
};
