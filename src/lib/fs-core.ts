import fs from 'fs';
import path from 'path';

const FILE_EXTENSION = 'json';

export const createFsCore = <DataType>(dirPath: string) => {
  return Object.freeze({
    write: async (name: string, data: DataType, extension = FILE_EXTENSION) => {
      try {
        await fs.promises.mkdir(dirPath, {
          recursive: true,
        });
        const files = await fs.promises.readdir(dirPath);
        const filename = files.find((file) => file.includes(name)) || `${name}.${extension}`;

        await fs.promises.writeFile(path.join(dirPath, filename), JSON.stringify(data, null, 2));

        return data as DataType;
      } catch (error) {
        throw Error('Unable to write to file :(');
      }
    },
    read: async (name: string, extension = FILE_EXTENSION) => {
      try {
        const data = await fs.promises.readFile(
          path.join(dirPath, `${name}.${extension}`),
          'utf-8',
        );
        return JSON.parse(data) as DataType;
      } catch (error) {
        throw Error('Unable to read a file :(');
      }
    },
    readSync: (name: string, extension = FILE_EXTENSION) => {
      try {
        const data = fs.readFileSync(path.join(dirPath, `${name}.${extension}`), {
          encoding: 'utf-8',
        });
        return JSON.parse(data) as DataType;
      } catch (error) {
        throw Error('Unable to read a file :(');
      }
    },
    scanDir: async () => {
      try {
        await fs.promises.mkdir(dirPath, { recursive: true });
        const files = await fs.promises.readdir(dirPath);
        return files;
      } catch {
        throw Error('Unable to scan a dir :(');
      }
    },
  });
};
