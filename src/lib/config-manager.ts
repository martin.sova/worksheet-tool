import path from 'path';

import deepMerge from 'deepmerge';
import { PDFOptions } from 'puppeteer';
import { IUserActivity } from '@/lib/user-activity-resolver';
import locales from '@/locales';
import { CONFIG_FILE } from '@/constants';

export interface IConfig {
  /** The base directory for resolving the pdf output */
  outDir: string;
  /** Preset of default values in the worksheet */
  dayDefaults: {
    hours: number;
    description: string;
    project: string;
  };
  /** Advanced puppeteer options */
  pdfOptions: Omit<PDFOptions, 'path'>;
  /** Configuration of external services for easier worksheet manipulation */
  services: {
    gitlab: {
      /** Gitlab api url eg. https://gitlab.example.com/api/v4  */
      url: string;
      /** Gitlab user name */
      user: string;
      /** Gitlab access token */
      token: string;
      /** Custom description resolver helps to create  your very own descrioption message */
      descriptionResolver?: (userActivity: IUserActivity) => string;
      /** Disable resolving description from gitlab */
      disableDescriptionResolver?: boolean;
      /** Like description resolver but for project  */
      projectResolver?: (userActivity: IUserActivity) => string;
      /** Like disableDescriptionRolver but for project  */
      disableProjectResolver?: boolean;
    };
  };
}

const ROOT_DIR_PATH = path.join(__dirname, '../../');

const DEFAULT_CONFIG: IConfig = {
  outDir: ROOT_DIR_PATH,
  dayDefaults: {
    hours: 8,
    description: '',
    project: '',
  },
  pdfOptions: {
    margin: {
      top: 30,
      right: 30,
      bottom: 30,
      left: 30,
    },
  },
  services: {
    gitlab: {
      user: '',
      url: '',
      token: '',
      disableDescriptionResolver: false,
      disableProjectResolver: false,
    },
  },
};

const createConfigManager = () => {
  let _config: IConfig | null = null;

  const _verify = () => {
    if (!_config) {
      try {
        const config = require(ROOT_DIR_PATH + CONFIG_FILE)?.default as (
          config: IConfig,
        ) => IConfig;
        _config = deepMerge((config && config(DEFAULT_CONFIG)) || {}, DEFAULT_CONFIG);
      } catch {
        console.warn(locales.configNotFound);
        _config = DEFAULT_CONFIG;
      }
    }
  };

  _verify();

  return Object.freeze({
    get config() {
      return _config as IConfig;
    },
  });
};

export const configManager = createConfigManager();
