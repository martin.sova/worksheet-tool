import path from 'path';
import { createId } from '@/utils/create-id';
import { PROFILE_DIR_NAME, SHEET_DIR_NAME, WORKSHEET_DIR_NAME } from '@/constants';
import { ISheetDate } from '@lib/sheet-manager';
import { createFsCore } from '@lib/fs-core';

const WORKSHEET_DIR_PATH = path.join(__dirname, `../../${WORKSHEET_DIR_NAME}`);

export interface IFileBaseProperties {
  _id: string;
}

export interface IProfile extends IFileBaseProperties {
  name: string;
  in: number;
  sheets: string[];
}

const createProfileFs = () => {
  const _profileDir = path.join(WORKSHEET_DIR_PATH, PROFILE_DIR_NAME);
  const fsCore = createFsCore<IProfile>(_profileDir);

  return Object.freeze({
    async set(id: string | null, data: Omit<IProfile, '_id'>) {
      const fileId = id || createId();
      return fsCore.write(fileId, { _id: fileId, ...data });
    },
    async get(id: string) {
      return fsCore.read(id);
    },
    async linkSheet(id: string, sheetId: string) {
      const profile = await this.get(id);
      return this.set(profile._id, {
        ...profile,
        sheets: [...profile.sheets, sheetId],
      });
    },
    async getList() {
      try {
        const files = await fsCore.scanDir();
        const profileList = await Promise.all(
          files.map(async (file) => {
            return await this.get(file.split('.')[0]);
          }),
        );
        return profileList;
      } catch (error) {
        throw Error();
      }
    },
  });
};

interface ISheet extends IFileBaseProperties {
  data: ISheetDate[];
}

const createSheetFs = () => {
  const _sheetDir = path.join(WORKSHEET_DIR_PATH, SHEET_DIR_NAME);
  const fsCore = createFsCore<ISheet>(_sheetDir);

  return Object.freeze({
    async set(id: string | null, data: ISheet['data']) {
      const fileId = id || createId();
      return fsCore.write(fileId, { _id: fileId, data });
    },
    async get(id: string) {
      return fsCore.read(id);
    },
  });
};

export const createWorkSheetFs = () => {
  const profileFsManager = createProfileFs();
  const sheetFsManager = createSheetFs();
  return Object.freeze({
    profile: profileFsManager,
    sheet: sheetFsManager,
  });
};
