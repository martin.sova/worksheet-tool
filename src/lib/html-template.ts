import path from 'path';
import fs from 'fs';
import handlebars from 'handlebars';
import { HTML_TEMPLATE } from '@/constants';
import { ISheetDate } from '@lib/sheet-manager';

interface ITemplateProps {
  name: string;
  in: number;
  date: string;
  md: number;
  hours: number;
  dates: ISheetDate[];
}

const ASSETS_DIR = path.join(__dirname, '../assets/');

export const createHtmlTemplate = () => {
  const getHTMLTemplate = async () => {
    try {
      const html = await fs.promises.readFile(ASSETS_DIR + HTML_TEMPLATE, 'utf8');
      return html;
    } catch (error) {
      throw Error('Html template could not be load :(');
    }
  };

  return Object.freeze({
    async createEncodedTemplate(templateProps: ITemplateProps) {
      const html = await getHTMLTemplate();
      const template = handlebars.compile<ITemplateProps>(html)(templateProps);
      const encodedTemplate = encodeURIComponent(template);
      return encodedTemplate;
    },
  });
};
