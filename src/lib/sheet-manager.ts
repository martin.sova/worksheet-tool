import { SheetDateType } from '@/utils/get-date-type';
import { generateDates, getDatesByType, ISheetDateBase } from '@/utils/sheet-date';
import { configManager } from '@lib/config-manager';

export interface ISheetDate extends ISheetDateBase {
  hours: number;
  project: string;
  description: string;
}

export const createSheetManager = (month: number, year: number) => {
  const { config } = configManager;

  const handleResolveSheetDate = (date: ISheetDateBase): ISheetDate => ({
    ...date,
    hours: config.dayDefaults.hours,
    project: config.dayDefaults.project,
    description: config.dayDefaults.description,
  });

  let _dates = generateDates<ISheetDate>(month, year, handleResolveSheetDate);

  return Object.freeze({
    get dates() {
      return _dates;
    },
    get holidays() {
      return getDatesByType(this.dates, SheetDateType.BANK_HOLIDAY);
    },
    get weekdays() {
      return getDatesByType(this.dates, SheetDateType.WEEKEND);
    },
    get workingDays() {
      return getDatesByType(this.dates, SheetDateType.WORK_DAY);
    },
    get totalHours() {
      return this.dates.reduce((total: number, { hours }: ISheetDate) => total + hours, 0);
    },
    get mds() {
      return this.totalHours / 8;
    },
    exclude(datesToExclude: string[]) {
      _dates = _dates.map((sheetDate) => {
        const hours = datesToExclude.includes(sheetDate.date) ? 0 : sheetDate.hours;

        return {
          ...sheetDate,
          project: hours ? sheetDate.project : '',
          description: hours ? sheetDate.description : '',
          hours: hours,
        };
      });
    },
    fillEntity(key: 'project' | 'description', date: string, description: string) {
      const dateIndex = _dates.findIndex(({ date: _date }) => _date === date);
      if (dateIndex !== -1) _dates[dateIndex][key] = description;
    },
    dangerouslyUpdateData(data: ISheetDate[]) {
      _dates = data;
    },
  });
};
