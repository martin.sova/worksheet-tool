import locales from '@/locales';
import { configManager } from '@lib/config-manager';

export enum ActivitySource {
  gitlab = 'gitlab',
}

export interface IUserActivity {
  createdAt: string;
  commitCount: number;
  action: string;
  ref: string;
  refType: string;
  commitMessage: string;
  projectName: string;
}

export const createUserActivityResolver = (source: ActivitySource, activity: IUserActivity[]) => {
  const { config } = configManager;

  const _resolveGitlabActivityMessage = ({ action, ref, commitMessage }: IUserActivity) => {
    if (ref) {
      if (action === 'pushed' && (ref.includes('DEV') || ref.includes('LEG'))) {
        return ref;
      }
      if (commitMessage && commitMessage.includes("into 'master'")) {
        return locales.activityMessages.release;
      }
      return locales.activityMessages.projectMaintaince;
    }

    return config.dayDefaults.description;
  };

  const _resolverMessage = (sourceType: ActivitySource, activity: IUserActivity) => {
    switch (sourceType) {
      case ActivitySource.gitlab: {
        const resolver = config?.services?.gitlab.descriptionResolver;
        return resolver ? resolver(activity) : _resolveGitlabActivityMessage(activity);
      }
      default: {
        return 'No source found!';
      }
    }
  };

  const _resolveGitlabActivityProject = ({ projectName }: IUserActivity) => {
    if (projectName) {
      return projectName;
    }

    return config.dayDefaults.project;
  };

  const _resolverProject = (sourceType: ActivitySource, activity: IUserActivity) => {
    switch (sourceType) {
      case ActivitySource.gitlab: {
        const resolver = config?.services?.gitlab.projectResolver;
        return resolver ? resolver(activity) : _resolveGitlabActivityProject(activity);
      }
      default: {
        return 'No source found!';
      }
    }
  };

  return Object.freeze({
    getResolvedActivity() {
      return activity.reduce(
        (data: Record<string, { project: string[]; message: string[] }>, activity) => {
          const resolvedProject = _resolverProject(source, activity);
          const resolvedMessage = _resolverMessage(source, activity);
          const { message, project } = data[activity.createdAt] || {
            message: [],
            project: [],
          };

          data[activity.createdAt] = {
            project: resolvedProject ? [...new Set([...project, resolvedProject])] : project,
            message: resolvedMessage ? [...new Set([...message, resolvedMessage])] : message,
          };

          return data;
        },
        {},
      );
    },
  });
};
