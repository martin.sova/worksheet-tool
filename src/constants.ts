export const CONFIG_FILE = 'sheet.config.ts';
export const HTML_TEMPLATE = 'template.html';
export const WORKSHEET_DIR_NAME = '.worksheet';
export const PROFILE_DIR_NAME = 'profile';
export const SHEET_DIR_NAME = 'sheet';
