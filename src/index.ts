import { createSpinner } from 'nanospinner';
import puppeteer from 'puppeteer';
import { createGitlabConnector } from '@lib/connector/gitlab.connector';
import { ActivitySource, createUserActivityResolver } from '@/lib/user-activity-resolver';
import { mapUserPushedEventsIntoCommonStructure } from '@/utils/adapters/user-pushed-events';
import { createId } from '@/utils/create-id';
import locales from '@/locales';
import { createHtmlTemplate } from '@lib/html-template';
import * as prompt from '@lib/prompt-questions';
import { createSheetManager } from '@lib/sheet-manager';
import { createWorkSheetFs, IProfile } from '@lib/worksheet-fs';
import { configManager } from '@lib/config-manager';

(async () => {
  const { config } = configManager;

  let profile: IProfile | undefined;

  const worksheetFs = createWorkSheetFs();
  const loadingSpinner = createSpinner();

  const profileList = await worksheetFs.profile.getList();

  if (profileList.length) {
    const { loadProfile } = await prompt.askLoadProfile([
      ...profileList.map((profile) => ({
        name: `${profile.name}, IČO: ${profile.in}`,
        id: profile._id,
      })),
      { name: locales.createNewItem, id: null },
    ]);

    if (loadProfile.id)
      profile = profileList.find((profile) => profile._id === loadProfile.id) as IProfile;
  }

  if (!profile) {
    const { name } = await prompt.askForName();
    const { IN } = await prompt.askForIN();

    profile = await worksheetFs.profile.set(null, {
      name,
      in: IN,
      sheets: [],
    });
  }

  const { year } = await prompt.askForYear();
  const { month } = await prompt.askForMonth();
  const sheet = createSheetManager(month - 1, year);

  const { excludedHolidays } = await prompt.askForWorkOverHolidays(sheet.holidays);
  const { excludedWeekends } = await prompt.askForWorkOverWeekends(sheet.weekdays);
  const { excludedDays } = await prompt.askForExcludeDays(sheet.workingDays);
  sheet.exclude([excludedHolidays, excludedWeekends, excludedDays].flat());

  if (config.services?.gitlab.url && config.services.gitlab.token && config.services.gitlab.user) {
    const { prefillGitlabActivity } = await prompt.askForPrefillDescriptionFromGitlab();
    if (prefillGitlabActivity) {
      loadingSpinner.start({ text: locales.fetchingGitlabData });

      const gitlabConnector = createGitlabConnector(month, year);
      const activity = await gitlabConnector.getUserPushedCommitEvents();

      if (activity.length) {
        const resolver = createUserActivityResolver(
          ActivitySource.gitlab,
          mapUserPushedEventsIntoCommonStructure(activity),
        );
        const resolvedActivity = resolver.getResolvedActivity();
        for (const date in resolvedActivity) {
          if (!config.services.gitlab.disableProjectResolver) {
            sheet.fillEntity('project', date, resolvedActivity[date].project.join(', '));
          }
          if (!config.services.gitlab.disableDescriptionResolver) {
            sheet.fillEntity('description', date, resolvedActivity[date].message.join(', '));
          }
        }

        loadingSpinner.stop({ text: locales.activityFound });
      } else {
        loadingSpinner.stop({ text: locales.activityNotFound });
      }
    }
  }

  const { editDataConfirm } = await prompt.askForEditDataConfirm();
  if (editDataConfirm) {
    try {
      const { editData } = await prompt.askForEditData(JSON.stringify(sheet.dates, null, 2));
      sheet.dangerouslyUpdateData(JSON.parse(editData));
    } catch {
      throw Error('invalid JSON data :(');
    }
  }

  const storedSheet = await worksheetFs.sheet.set(null, sheet.dates);
  await worksheetFs.profile.linkSheet(profile._id, storedSheet._id);

  loadingSpinner.start({ text: locales.generatingPDF });
  const template = createHtmlTemplate();
  const html = await template.createEncodedTemplate({
    name: profile.name,
    in: profile.in,
    date: `${month}.${year}`,
    md: sheet.mds,
    hours: sheet.totalHours,
    dates: sheet.dates,
  });

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.goto(`data:text/html;charset=UTF-8,${html}`, {
    waitUntil: 'networkidle0',
  });

  await page.pdf({
    format: 'a4',
    path: `${config.outDir}/${profile.name}-${month}-${year}.${createId()}.pdf`,
    printBackground: true,
    ...config.pdfOptions,
  });

  await browser.close();
  loadingSpinner.stop({ text: locales.success });
})();
