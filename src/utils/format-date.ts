export const formatDate = (date: Date) => date.toISOString().replace(/T.*/, ''); // YYYY-MM-DD
