export const getUTCDate = (month: number, year: number, day = 1) =>
  new Date(Date.UTC(year, month, day));
