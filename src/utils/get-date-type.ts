export enum SheetDateType {
  WEEKEND = 'weekend',
  BANK_HOLIDAY = 'bank-holiday',
  DAY_OFF = 'day-off',
  WORK_DAY = 'work-day',
}

export const getDateType = (date: Date, holidays: Date[]) => {
  const day = date.getDay();

  if (day === 0 || day === 6) return SheetDateType.WEEKEND;
  if (holidays.some((holiday) => holiday.getDate() === date.getDate()))
    return SheetDateType.BANK_HOLIDAY;
  else return SheetDateType.WORK_DAY;
};
