import { AxiosResponse } from 'axios';
type IFetcher<Results> = (page: number) => Promise<AxiosResponse<Results[]>>;

export async function* itarableApiResultsGenerator<Results>(fetcher: IFetcher<Results>) {
  let lastResponseLength = null;
  let page = 0;
  try {
    do {
      const result = await fetcher(page);
      yield result;
      lastResponseLength = result?.data.length || 0;
      page = page + 1;
    } while (lastResponseLength !== 0);
  } catch (e) {
    console.error(e);
  }
}

export const getIterableApiResults = async <Results>(api: IFetcher<Results>) => {
  const packedData = [];
  for await (const response of itarableApiResultsGenerator(api)) {
    packedData.push(...response.data);
  }
  return packedData;
};
