import { formatDate } from '@/utils/format-date';
import { getDateType, SheetDateType } from '@/utils/get-date-type';
import { getHolidayDatesForCurrentMonth } from '@/utils/get-holiday-dates-for-current-month';
import { getUTCDate } from '@/utils/get-utc-date';

export interface ISheetDateBase {
  key: number;
  type: SheetDateType;
  date: string;
}

export const getDatesByType = <T extends ISheetDateBase>(dates: T[], type: SheetDateType) =>
  dates.filter((date) => date.type === type).map((date) => date.date);

export const generateDates = <T extends ISheetDateBase>(
  month: number,
  year: number,
  resolver?: (date: ISheetDateBase) => T,
) => {
  const current = getUTCDate(month, year);
  const holidays = getHolidayDatesForCurrentMonth(current);

  const dates = [];
  while (current.getUTCMonth() === month) {
    const date: ISheetDateBase = {
      key: current.getTime(),
      type: getDateType(current, holidays),
      date: formatDate(current),
    };

    dates.push(resolver ? resolver(date) : date);
    current.setUTCDate(current.getUTCDate() + 1);
  }
  return dates as T[];
};
