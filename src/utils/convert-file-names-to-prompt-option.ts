import locales from '@/locales';

export const convertFileNamesToPromptOption = (files: string[]) =>
  files.map((file) => {
    const [name, id] = file.split('.');
    return {
      name: `${name}, ${locales.createdAt} ${new Date(+id)}`,
      fileName: file,
    };
  });
