import { IUserActivity } from '@/lib/user-activity-resolver';
import { IGitlabUserEvent } from '@lib/connector/gitlab.connector';

export const mapUserPushedEventsIntoCommonStructure = (
  events: IGitlabUserEvent[],
): IUserActivity[] =>
  events.map((event) => ({
    commitCount: event.push_data.commit_count || 0,
    action: event.push_data.action,
    createdAt: event.created_at.split('T')[0], // YYYY-MM-DD
    refType: event.push_data.ref_type,
    ref: event.push_data.ref,
    commitMessage: event.push_data.commit_title,
    projectName: event.project_name,
  }));
