export enum Locale {
  cs = 'cs',
  en = 'en',
}

export const getLocale = () => {
  const env = process.env;
  const locale = (env.LC_ALL || env.LC_MESSAGES || env.LANG || env.LANGUAGE || 'cs_CZ.UTF-8').split(
    '_',
  )[0];

  switch (locale) {
    case Locale.cs:
      return Locale.cs;
    case Locale.en:
      return Locale.en;
    default:
      return Locale.cs;
  }
};
