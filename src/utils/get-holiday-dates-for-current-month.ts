/**
 * @warn - Cannot be handle like this!
 */

const BANK_HOLIDAYS = [
  '1.1.',
  '1.1.',
  '15.4.',
  '18.4.',
  '1.5.',
  '8.5.',
  '5.7.',
  '6.7.',
  '28.9.',
  '28.10.',
  '17.11.',
  '24.12.',
  '25.12.',
  '26.12.',
];

export const getHolidayDatesForCurrentMonth = (date: Date) => {
  return BANK_HOLIDAYS.map((holiday) => {
    const [d, m] = holiday.split('.');
    return new Date(Date.UTC(date.getFullYear(), +m - 1, +d));
  }).filter((holiday) => date.getMonth() === holiday.getMonth());
};
