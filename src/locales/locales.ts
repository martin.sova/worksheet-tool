export interface Locales {
  promptQuestions: {
    saveProfile: string;
    name: string;
    in: string;
    year: string;
    month: string;
    selectHolidays: string;
    workOverWeekends: string;
    selectWeekends: string;
    shouldExcludeDays: string;
    workOverHolidays: (count: number) => string;
    profile: (count: number) => string;
    prefillGitlabActivity: string;
    editModeConfirm: string;
  };
  activityMessages: {
    projectMaintaince: string;
    release: string;
  };
  createNewItem: string;
  createdAt: string;
  somethingWentWrong: string;
  generatingPDF: string;
  success: string;
  fetchingGitlabData: string;
  activityFound: string;
  activityNotFound: string;
  configNotFound: string;
}

export const cs: Locales = {
  promptQuestions: {
    profile: (count: number) => `Bylo nalezeno ${count} profilů, zvol další akci.`,
    saveProfile: 'Přeješ si uložit údaje pro další použití?',
    name: 'Zadej jméno vlastníka výkazu.',
    in: 'Zadej IČO.',
    year: 'Vyber rok.',
    month: 'Vyber měsíc.',
    workOverHolidays: (count: number) =>
      `Byly nalezeny ${count} státní svátky, přeješ si je zahrnout do výkazu, jako pracovní dny?`,
    selectHolidays: 'Vyber které dny si přeješ zahrnout do výkazu.',
    workOverWeekends: 'Přeješ si zahrnout některý z víkendových dnů?',
    selectWeekends: 'Vyber si které dny si přeješ zahrnout do výkazu.',
    shouldExcludeDays: 'Dopřál sis nějaké volno? sick day, dovolená apod...?',
    prefillGitlabActivity: 'Přeješ si předvyplňit výkaz gitlab aktivitou?',
    editModeConfirm:
      'Přeješ si upravit vstupní soubor výkazu? (můžeš si upravit co chceš, ale můžeš to rozbít :))',
  },
  activityMessages: {
    projectMaintaince: 'údržba systémů',
    release: 'release prod. prostředí',
  },
  createNewItem: 'Vytvořit nový..',
  createdAt: 'Vytvořeno',
  somethingWentWrong: 'Něco se pokazilo :(',
  generatingPDF: 'Generuji výkaz..',
  success: 'Výkaz byl úspěšně vytvořen.',
  fetchingGitlabData: 'Stahuji gitlab aktivitu.',
  activityFound: 'Aktivita nalezena!',
  activityNotFound: 'Žádná aktivity nebyla nalezena :(',
  configNotFound: 'Konfigurační soubor nenalezen :(',
};

export const en: Locales = {
  promptQuestions: {
    profile: (count: number) => `${count} profile was found, please select next action.`,
    saveProfile: 'Would you like to save the profile?',
    name: 'Enter full name.',
    in: 'Enter IN.',
    year: 'Select year.',
    month: 'Select month.',
    workOverHolidays: (count: number) =>
      `${count} bank-holidays were found. Would you like to include them in the report as a working days?`,
    selectHolidays: 'Select which days you want to include in the report.',
    workOverWeekends: 'Would you like to include some weekend days?',
    selectWeekends: 'Select which days you want to include in the report.',
    shouldExcludeDays: 'Did you take some time off? sick day, vacation etc...?',
    prefillGitlabActivity: 'Would you like to prefill worksheet from gitlab activity?',
    editModeConfirm:
      'Do you want to edit input file? (you can edit what you want, but you can break it :))',
  },
  activityMessages: {
    projectMaintaince: 'project maintaince',
    release: 'production release',
  },
  createNewItem: 'Create new..',
  createdAt: 'Created at',
  somethingWentWrong: 'Something went wrong :(',
  generatingPDF: 'Generating report...',
  success: 'The report was successfully generated!',
  fetchingGitlabData: 'Fetching gitlab actitivy.',
  activityFound: 'Activity found!',
  activityNotFound: "Activity wasn't found :(",
  configNotFound: 'Configuration file not found :(',
};
