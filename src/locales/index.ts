import { getLocale, Locale } from '@/utils/get-locale';

import { cs, en } from './locales';

const LocalesMap = {
  [Locale.cs]: cs,
  [Locale.en]: en,
};

export default LocalesMap[getLocale()];
