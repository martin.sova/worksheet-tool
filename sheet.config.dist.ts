import { IConfig } from '@lib/config-manager';

const config = (config: IConfig): IConfig => {
  // Output directiory path
  // config.outDir = ''

  // Default strings for each work day
  // config.dayDefaults = {
  //   project: '',
  //   description: '',
  //   hours: 8,
  // };

  // Gitlab service
  // config.services.gitlab = {
  //   url: 'URL',
  //   user: 'USER',
  //   token: 'TOKEN',
  //   disableProjectResolver: false,
  //   disableDescriptionResolver: false,
  //   projectResolver: (activity) => {
  //     switch (activity.projectName) {
  //       case 'example':
  //         return 'EX';
  //       default:
  //         return '';
  //     }
  //   },
  //   descriptionResolver: (activity) => {
  //     return ''
  //   },
  // };

  return config;
};

export default config;
