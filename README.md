# WorkSheet

je jednouchý nástroj pro generování výkazů pro firmu.

**Stáhne závislosti**

`$ yarn`

**Spustí script pro vygenerování výkazu**

`$ yarn worksheet:generate`

## Konfigurace

Nástroj lze základně nakonfigurovat skrze konfigurační soubor `sheet.config.ts`.


### Základní konfigurace
|parametr |typ |popis |
|-------------------------|--------|------------------------------------------------------------------|
| outDir | string | složka kam se má výkaz vygenerovat. |
| dayDefaults.hours | number | přednastavení výchozího počtu pracovních hodin pro každý den. |
| dayDefaults.description | string | přednastavení výchozího popisku pro každý pracovní den. |
| dayDefaults.project | string | přednastavení výchozího projektu pro každý pracovní den |
| pdfOptions | object | https://puppeteer.github.io/puppeteer/docs/puppeteer.pdfoptions/ |


### Předvyplnění výkazu prostřednitcvím GitLab aktivity
Předvyplnění výkazu je možné pouze za předpokladu vyplnění klíčových parametrů služby a to sice `url`, `user`, `token`.
|parametr |typ |popis |
|-------------------------|--------|------------------------------------------------------------------|
| services.gitlab.url | string | GitLab api např.: url https://gitlab.example.com/api/v4 |
| services.gitlab.user | string | Jméno uživatele v GitLabu |
| services.gitlab.token | string | GitLab Personal access token |


### Příklad konfiguračního souboru (sheet.config.dist.ts)

    // sheet.config.ts
    import { IConfig } from  '@lib/config-manager';

    const  config  = (config:  IConfig):  IConfig  => {
        config.outDir = ''

       config.dayDefaults  = {
           project:  '',
    	   description:  '',
    	   hours:  8,
       };

      config.services.gitlab  = {
    	  url:  'URL',
    	  user:  'USER',
    	  token:  'TOKEN',
    	  disableProjectResolver:  false,
    	  disableDescriptionResolver:  false,
    	  projectResolver: (activity) => {
    		  switch (activity.projectName) {
    			  case  'example': {
    				  return  'EX';
    			  }
    		default:
    			return  '';
    		}
    	},
    	descriptionResolver: (activity) => {
    		return  ''
    	},
    };

    export  default  config;
